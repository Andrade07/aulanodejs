const express = require('express');
const app = express();
const path = require('path');
const port = 3000;

app.use(express.static('public'));

// Configuração do mecanismo de visualização EJS
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('layout', { pagina:'home', body: 'Conteúdo da página Home...' });
});

app.get('/objetivos', (req, res) => {
  res.render('layout', { pagina:'objetivos', body: 'Conteúdo da página objetivos...' });
});

app.get('/desenvolvimento', (req, res) => {
    res.render('layout', { pagina:'desenvolvimento', body: 'Conteúdo da página desenvolvimento...' });
});

app.get('/conclusao', (req, res) => {
    res.render('layout', { pagina:'conclusao', body: 'Conteúdo da página conclusao...' });
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});
